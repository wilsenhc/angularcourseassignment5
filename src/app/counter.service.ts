import { Injectable, OnInit } from '@angular/core';

@Injectable()
export class CounterService implements OnInit {
  activeToInactive: number = 0;
  inactiveToActive: number = 0;

  constructor() { }

  ngOnInit() {
    this.activeToInactive = 0;
    this.inactiveToActive = 0;
  }

  toActive() {
    this.inactiveToActive++;
    console.log(this.inactiveToActive);
  }

  toInactive() {
    this.activeToInactive++;
    console.log(this.activeToInactive);
  }
}
